#!/usr/bin/env perl
use strict;
use warnings;
use CPAN;
use FindBin qw($Bin);
use local::lib "$Bin/lib";

CPAN::install("JSON");
CPAN::install("File::Slurp");
