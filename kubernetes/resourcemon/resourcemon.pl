#!/usr/bin/env perl
use strict;
use warnings;
use v5.10;
use POSIX;
use JSON;
use File::Slurp;


my $storagedir = "data";


my %hide = (
    "crossplane-system" => 1,
    "kube-system" => 1,
    "openshift-apiserver" => 1,
    "openshift-apiserver-operator" => 1,
    "openshift-authentication-operator" => 1,
    "openshift-cloud-controller-manager-operator" => 1,
    "openshift-cloud-credential-operator" => 1,
    "openshift-cloud-network-config-controller" => 1,
    "openshift-cluster-csi-drivers" => 1,
    "openshift-cluster-machine-approver" => 1,
    "openshift-cluster-node-tuning-operator" => 1,
    "openshift-cluster-samples-operator" => 1,
    "openshift-cluster-storage-operator" => 1,
    "openshift-cluster-version" => 1,
    "openshift-config-operator" => 1,
    "openshift-console" => 1,
    "openshift-console-operator" => 1,
    "openshift-controller-manager" => 1,
    "openshift-controller-manager-operator" => 1,
    "openshift-dns" => 1,
    "openshift-dns-operator" => 1,
    "openshift-etcd" => 1,
    "openshift-etcd-operator" => 1,
    "openshift-image-registry" => 1,
    "openshift-ingress" => 1,
    "openshift-ingress-canary" => 1,
    "openshift-ingress-operator" => 1,
    "openshift-insights" => 1,
    "openshift-kube-apiserver" => 1,
    "openshift-kube-apiserver-operator" => 1,
    "openshift-kube-controller-manager" => 1,
    "openshift-kube-controller-manager-operator" => 1,
    "openshift-kube-scheduler" => 1,
    "openshift-kube-scheduler-operator" => 1,
    "openshift-kube-storage-version-migrator" => 1,
    "openshift-kube-storage-version-migrator-operator" => 1,
    "openshift-machine-api" => 1,
    "openshift-machine-config-operator" => 1,
    "openshift-marketplace" => 1,
    "openshift-monitoring" => 1,
    "openshift-multus" => 1,
    "openshift-network-diagnostics" => 1,
    "openshift-network-operator" => 1,
    "openshift-oauth-apiserver" => 1,
    "openshift-operator-lifecycle-manager" => 1,
    "openshift-operators" => 1,
    "openshift-pipelines" => 1,
    "openshift-sdn" => 1,
    "openshift-service-ca" => 1,
    "openshift-service-ca-operator" => 1,
    );


my $epoch = time();
my $date = POSIX::strftime("%Y-%m-%d-%H:%M:%S", localtime($epoch));
mkdir($storagedir);

chomp(my @x = `oc adm top pods -A | grep -v NAMESPACE`);
foreach my $l (@x) {
    (my $ns, my $podname, my $cpu, my $mem) = split(/\s+/, $l);
    unless($hide{$ns}) {
	my $podinfo = `oc get pod $podname -n $ns -o json`; # | jq -c '.spec.containers[] | .resources'
	my $json = decode_json $podinfo;

	# fixme: not sure what happens if a pod has many containers running at the same time
	foreach my $c (@{$json->{spec}->{containers}}) {
	    my $result = JSON->new->utf8->canonical->encode({reqcpu => $c->{resources}->{requests}->{cpu} // "",
							     reqmem => $c->{resources}->{requests}->{memory} // "",
							     limcpu => $c->{resources}->{limits}->{cpu} // "",
							     limmem => $c->{resources}->{limits}->{memory} // "",
							     usecpu => $cpu,
							     usemem => $mem,
							     ns => $ns,
							     name => $podname,
							     localtime => $epoch,
							     time => $date });
	    append_file($storagedir.'/'.$ns.'---'.$podname, $result,"\n");
	}
    }
}
