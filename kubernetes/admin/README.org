* create ldap objects
1. login to https://ldap.jobtechdev.se/
2. create a Generic Simple Security Object under
  ou=testusers,ou=developers,ou=users,dc=jobtechdev,dc=se
3. select username and password

* create openshift objects
: oc project openshift
: oc apply -f cluster-readonly-no-labels-or-annotations.yaml

Enter the ldap username into ~role-binding.yaml~ and run:
: oc apply -f role-binding.yaml
